#!/usr/bin/env python


from setuptools import setup
import picoharpds


def main():

    setup(
        name='tangods-picoharpds',
        version=picoharpds.__version__,
        package_dir={'picoharpds': 'picoharpds'},
        packages=['picoharpds'],
        include_package_data=True,  # include files in MANIFEST
        author='Jairo Moldes',
        author_email='jmoldes@cells.es',
        description='Tango device server for controlling a picoharp 300 '
                    'photon counter',
        license='GPLv3+',
        platforms=['src', 'noarch'],
        url='https://git.cells.es/controls/tangods-ph300photoncounter.git',
        requires=['tango (>=7.2.6)'],
        entry_points={
            'console_scripts': [
                'PH_PhotonCounter = picoharpds.ph_photoncounter:main',
            ],
        },
    )


if __name__ == "__main__":
    main()
